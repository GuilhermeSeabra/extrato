function carregarItens() {
  var idhosp = "";
  var btn = "";
  var arr = [];
  var itens = "";
  var dados = [];

  var currentUrl = window.location.href;
  var objUrl = new URL(currentUrl);
  var r = objUrl.searchParams.get("x");

  console.log(r);

  var url = "http://172.30.29.11:8888/change/extrato.php?x=" + r;

  $.ajax({
    url: url,
    cache: false,
    dataType: "json",
    beforeSend: function () {
      $("h2").html("carregando informações...");
    },
    error: function () {
      $("h2").html("Erro ao carregar informações");
    },
    success: function (retorno) {

      for (var i = 0; i < retorno.IDCONTA.length; i++) {
        dados.push({
          id: retorno.IDCONTA[i],
          name: retorno.DESIGNACAO[i],
          lastName: retorno.SOBRENOME[i],
          date: retorno.DATALANCAMENTO[i],
          hour: retorno.HORALANCA[i],
          desc: retorno.DESCRICAO[i],
          value: retorno.VLRLANCAMENTO[i],
          apto: retorno.CODUH[i],
          reservation: retorno.NUMRESERVA[i],
          checkIn: retorno.DATACHEGADA[i],
          checkOut: retorno.DATAPARTIDA[i]
        });
      }

      // separa os ids e nome unicos para mostrar nos botões de seleção.
      var flags = [],
        output = [],
        l = dados.length,
        i;
      for (i = 0; i < l; i++) {
        if (flags[dados[i].lastName + dados[i].id + dados[i].name]) continue;
        flags[dados[i].lastName + dados[i].id + dados[i].name] = true;
        output.push([dados[i].lastName, dados[i].id, dados[i].name]);
      }

      // ao clicar no botão oculta a tela de selecionar e mostra a tela de 
      function myFunction() {
        var element = document.getElementById("consumos");
        var element2 = document.getElementById("container");
        element.classList.toggle("none");
        element2.classList.toggle("none");
      }

      // pega os nomes unicos e joga nos botões na tela
      for (var i = 0; i < output.length; i++) {
        btn += "<button id=\"btnn\" class=\"botao\" value=\"" + output[i][1] + "\">" + output[i][2] + "</button>"
      }
      $("#btn").html(btn);

      // botão de seleção de nome/conta
      setTimeout(function () {
        $(".botao").click(function () {
          myFunction();
          idhosp = this.value;

          // faz o filter quando seleciona o nome e joga no arr
          arr = agrupa(dados, idhosp);
          exibeConta();

        });
      }, 100); // segundos

      // volta da tela de extrato para a tela de seleção
      $("#voltar").click(function () {
        myFunction();
        var botao = document.getElementById("btnn");
        location.reload();
      })

      // função para criar um arr com os id do nome selecionado
      function agrupa(arr, refId) {
        return arr.filter(function (i) {
          return i.id == refId;
        });
      };

      function exibeConta() {
        var nome, uh, reserva, chegada, saida;
        var valor = [];
        for (var i = 0; i < arr.length; i++) {

          itens += "<tr>";
          // itens += "<td>" + arr[i].id + "</td>";
          itens += "<td>" + modelaData(arr[i].date) + " " + modelaHora(arr[i].hour) + "</td>";
          itens += "<td>" + arr[i].desc + "</td>";
          itens += "<td>" + "R$ " + "<span id=\"vlr\">" + modelaValor(arr[i].value) + "</span>" + "</td>";
          itens += "</tr>";

          nome = arr[i].name;
          uh = arr[i].apto;
          reserva = arr[i].reservation;
          chegada = arr[i].checkIn;
          saida = arr[i].checkOut;

          valor.push(arr[i].value);

        }

        // Faz a soma do total de lançamentos
        function somaValores() {
          var soma = 0;
          var val = []
          for (var i = 0; i < valor.length; i++) {
            val[i] = parseInt(valor[i]);
            soma += parseInt(val[i]);
          }
          return soma;
        }

        // converte o valor e coloca as casas decimais
        function converteTotal() {
          var valor = somaValores().toString().split(".");

          if (valor.length == 1) {
            valor += ",00"
          }

          if (valor[1].length == 1) {
            valor[1] += "0"
          }

          if (valor[0].length == 1) {
            valor[0] = "&nbsp&nbsp" + valor[0]
          }

          if ((valor[0].length + valor[1].length) > 2) {
            valor = valor[0] + "," + valor[1];
          }

          return valor;
        }

        // Muda o formato da data
        // TODO: terminar com todos os case
        function modelaData(date) {
          var fullDate = date.split("-");
          var mes = "";

          switch (fullDate[1]) {
            case "OCT":
              mes = 10;
              break;
          }

          return fullDate[0] + "/" + mes + "/" + fullDate[2];
        }

        // Tira os segundos da hora.
        function modelaHora(time) {
          var fullTime = time.split(":");
          return fullTime[0] + ":" + fullTime[1];
        }

        // modela valor e coloca casas decimais caso não tenha
        function modelaValor(value) {
          var fullValue = value.split(".");

          if (fullValue.length == 1) {
            fullValue += ",00"
          }

          if (fullValue[1].length == 1) {
            fullValue[1] += "0"
          }

          if (fullValue[0].length == 1) {
            fullValue[0] = "&nbsp&nbsp" + fullValue[0]
          }

          return fullValue;
        };



        $("tbody").html(itens);
        $("h2").html("");
        $("#nome").html(nome);
        $("#uh").html(uh);
        $("#reserva").html(reserva);

        if (chegada == null) {
          $("#chegada").html('Não informado');
        } else {
          $("#chegada").html(modelaData(chegada));
        }

        if (saida == null) {
          $("#partida").html('Não informado');
        } else {
          $("#partida").html(modelaData(saida));
        }

        $("#total").html(converteTotal());


      }

    }
  });
};